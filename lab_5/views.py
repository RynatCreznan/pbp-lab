from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from lab_5.models import MyWatchlist
from lab_5.forms import FilmForms
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    film = MyWatchlist.objects.all()
    response = {'list_film': film}
    return render(request, 'lab5_index.html', response)

def watchlist(request):
    film = MyWatchlist.objects.all()
    response = {'list_film': film}
    return render(request, 'lab5_watchlist.html', response)

@login_required(login_url= '/admin/login/')
def add_film(request):
    form_film = FilmForms(request.POST or None, request.FILES or None)
    if request.method == "POST":
        if form_film.is_valid():
            form_film.save()
            return HttpResponseRedirect('/lab-5/watchlist')
    response = {'form' : form_film}
    return render(request, 'lab5_form.html', response)
