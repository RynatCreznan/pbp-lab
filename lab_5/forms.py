from django import forms
from lab_5.models import MyWatchlist

class FilmForms(forms.ModelForm):
    class Meta:
        model = MyWatchlist
        fields = "__all__"
    widgets = {
            "watched" : forms.CheckboxInput(), 
            "title" : forms.TextInput(),
            "rating" : forms.NumberInput(),
            "release_date" : forms.TextInput(),
            "review" : forms.TextInput(),
        }