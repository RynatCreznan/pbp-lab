from django.shortcuts import render
from .models import MyWatchList
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    WatchLists = MyWatchList.objects.all()  
    response = {'WatchLists': WatchLists}
    return render(request, 'lab3.html', response)

def xml(request):
    data = serializers.serialize('xml', MyWatchList.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', MyWatchList.objects.all())
    return HttpResponse(data, content_type="application/json")
