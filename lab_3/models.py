from django.db import models

# Create your models here.
class MyWatchList(models.Model):
    watched = models.BooleanField(default=False)
    title = models.CharField(max_length=30, default=" ")
    rating = models.FloatField(default="0.0")
    release_date = models.TextField(default=" ")
    review = models.TextField(max_length=255, default=" ") 
